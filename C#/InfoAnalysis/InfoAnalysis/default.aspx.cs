﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace InfoAnalysis
{
    public partial class _default : System.Web.UI.Page
    {
        //path for files
        public static string dataPath = HttpContext.Current.Server.MapPath(".") + @"\data\";
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        [WebMethod]
        public static string GetData(string request)
        {
            //opens reader
            StreamReader reader = new StreamReader(dataPath + "Analysis.txt");
            string line = reader.ReadLine();//sets line to string
            reader.Close();//closes reader
            return line;//returnd record/line
        }

        [WebMethod]
        public static string SaveData(string request)
        {
            //gets file
            StreamWriter write = new StreamWriter(dataPath + "Analysis.txt");
            write.WriteLine(request);//writes string to file
            write.Close();//closes file
            return "DataSaved";//reutrns string for info to AJAX
        }
    }
}