﻿//object to use
var analysis = {
    probability : 0,
    severity : 0,
    riskResult : "",
    reputational : 0,
    regulatory : 0,
    riskStatus : 0
};
var hostPage = "default.aspx/";//used for calling the ajax function


//helper method
function $gel(el) {
    return document.getElementById(el);
}

//inital method
function init()
{
    //loads on startup
    loadCombos();
    callAJAX("GetData", "");
    $gel("btnSubmit").disabled = true;
    
}

function loadCombos() {
    //load all comboboxes
    $gel("cmbProbability").options = 0;
    $gel("cmbProbability").options[0] = new Option(1, 0);
    $gel("cmbProbability").options[1] = new Option(2, 1);
    $gel("cmbProbability").options[2] = new Option(3, 2);
    $gel("cmbProbability").options[3] = new Option(4, 3);
    $gel("cmbProbability").options[4] = new Option(5, 4);

    $gel("cmbSeverity").options = 0;
    $gel("cmbSeverity").options[0] = new Option(1, 0);
    $gel("cmbSeverity").options[1] = new Option(2, 1);
    $gel("cmbSeverity").options[2] = new Option(3, 2);

    $gel("cmbRepRisk").options = 0;
    $gel("cmbRepRisk").options[0] = new Option("High - Global", 0);
    $gel("cmbRepRisk").options[1] = new Option("Medium - Reginal", 1);
    $gel("cmbRepRisk").options[2] = new Option("Low - Local", 2);

    $gel("cmbRegulatory").options = 0;
    $gel("cmbRegulatory").options[0] = new Option("N/A", 0);
    $gel("cmbRegulatory").options[1] = new Option("Yes", 1);
    $gel("cmbRegulatory").options[2] = new Option("No", 2);

    $gel("cmbRiskStat").options = 0;
    $gel("cmbRiskStat").options[0] = new Option("Red", 0);
    $gel("cmbRiskStat").options[1] = new Option("Amber", 1);
    $gel("cmbRiskStat").options[2] = new Option("Green", 2);
    
    $gel("txtRiskResult").maxLength = 30;//sets the max length of the text box

}


function loadForm(json)
{
    //loads each form element with data
    $gel("cmbProbability").selectedIndex = json.probability;
    $gel("cmbSeverity").selectedIndex = json.severity;//options[json.severity];// 
    $gel("txtRiskResult").value = json.riskResult;
    $gel("cmbRepRisk").selectedIndex = json.reputational;
    $gel("cmbRegulatory").selectedIndex = json.regulatory;
    $gel("cmbRiskStat").selectedIndex = json.riskStatus;
    riskChange($gel("cmbRiskStat"));
}


function save()
{
    //saves all info to object analysis
    analysis.probability = $gel("cmbProbability").options[$gel("cmbProbability").selectedIndex].value;
    analysis.severity = $gel("cmbSeverity").options[$gel("cmbSeverity").selectedIndex].value;
    analysis.riskResult = $gel("txtRiskResult").value;
    analysis.reputational = $gel("cmbRepRisk").options[$gel("cmbRepRisk").selectedIndex].value;
    analysis.regulatory = $gel("cmbRegulatory").options[$gel("cmbRegulatory").selectedIndex].value;
    analysis.riskStatus = $gel("cmbRiskStat").options[$gel("cmbRiskStat").selectedIndex].value;
    
    $gel("btnSubmit").disabled = false;
}

function submitForm()
{
    var json = JSON.stringify(analysis);

    callAJAX("SaveData", json);
    
    $gel("btnSubmit").disabled = true;
    

}

//bacground change on risk combobox
function riskChange(select)
{
    var sel = select.options[select.selectedIndex].text;
    
    switch (sel) {
        case "Red":
            $gel("cmbRiskStat").style.backgroundColor = sel;
            break;
        case "Amber":
            $gel("cmbRiskStat").style.backgroundColor = "#FFBF00";
            break;
        case "Green":
            $gel("cmbRiskStat").style.backgroundColor = sel;
            break;
    }
        
}

//AJAX function
function callAJAX(requestMethod, clientRequest) {

    var pageMethod = hostPage + requestMethod;
    //var jData = JSON.stringify({ request: clientRequest });
       

    $.ajax({
        url: pageMethod,
        data: JSON.stringify({ request: clientRequest }), //request is the var in the c# method
        type: "POST", // data has to be POSTed  
        contentType: "application/json", // posting JSON content      
        dataType: "JSON",  // type of data is JSON (must be upper case!)  
        timeout: 600000,    // AJAX timeout  
        success: function (result) {
            ajaxCallback(result.d);
        },
        error: function (xhr, status) {
            alert(status + " - " + xhr.responseTest);
        }

    });
}

//ajax info received from c# method
function ajaxCallback(serverResponse) {
    //if not DataSaved then parses the json file and sends it to loadForm()
    if (serverResponse !== "DataSaved") {
        analysis = JSON.parse(serverResponse);
        loadForm(analysis);
    }
    else
    {
        alert("Data saved to file on server.");
    }
    
}
