﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="InfoAnalysis._default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Risk Analysis</title>
    <link rel="stylesheet" href="styles/style.css" />
    <script src="scripts/jquery-2.1.3.js"></script>
    
    <script src="scripts/script.js"></script>
</head>
<body onload="init();">
    <header>
    <h1>Add Risk - Analysis</h1>
    <p>Does generate provision meet engineering standards?</p>
        </header>
    <form id="form1" runat="server">
        <div id="frmMain">
            <label>Probability</label>
            <select id="cmbProbability"></select><br /><br />
            <label>Severity</label>
            <select id="cmbSeverity"></select><br /><br />
            <label>Risk Result</label>
            <input type="text" id="txtRiskResult" /><br /><br />
            <label>Reputational Risk</label>
            <select id="cmbRepRisk"></select><br /><br />
            <label>Regulatory</label>
            <select id="cmbRegulatory"></select><br /><br />
            <label>Risk Status</label>
            <select id="cmbRiskStat" onchange="riskChange(this);"></select><br /><br /><br />
            <hr />
             <input type="button" id="btnSubmit" value="Submit" onclick="submitForm();" />
            <input type="button" id="btnSave" value="Save" onclick="save();"/>
             <input type="button" id="btnNext" value="Next" onclick="" />
            <input type="button" id="btnPrev" value="Previous" onclick="" />
           
            
           
            <br /> <br /> <br />
            <div id="test"></div>

        </div>
    </form>
</body>
</html>
