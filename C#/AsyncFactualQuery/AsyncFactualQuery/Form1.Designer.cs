﻿namespace AsyncFactualQuery
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtQueryStatus = new System.Windows.Forms.TextBox();
            this.txtJsonData = new System.Windows.Forms.TextBox();
            this.txtEditWindow = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.btnDone = new System.Windows.Forms.Button();
            this.btnStartQueries = new System.Windows.Forms.Button();
            this.btnStartFactual = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtQueryStatus
            // 
            this.txtQueryStatus.Location = new System.Drawing.Point(9, 24);
            this.txtQueryStatus.Multiline = true;
            this.txtQueryStatus.Name = "txtQueryStatus";
            this.txtQueryStatus.Size = new System.Drawing.Size(583, 109);
            this.txtQueryStatus.TabIndex = 0;
            // 
            // txtJsonData
            // 
            this.txtJsonData.Location = new System.Drawing.Point(9, 166);
            this.txtJsonData.Multiline = true;
            this.txtJsonData.Name = "txtJsonData";
            this.txtJsonData.Size = new System.Drawing.Size(583, 109);
            this.txtJsonData.TabIndex = 1;
            // 
            // txtEditWindow
            // 
            this.txtEditWindow.Location = new System.Drawing.Point(9, 308);
            this.txtEditWindow.Multiline = true;
            this.txtEditWindow.Name = "txtEditWindow";
            this.txtEditWindow.Size = new System.Drawing.Size(583, 109);
            this.txtEditWindow.TabIndex = 2;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(68, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Query Status";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 150);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "JSON Data";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 292);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(111, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Editable Text Window";
            // 
            // btnDone
            // 
            this.btnDone.Location = new System.Drawing.Point(500, 425);
            this.btnDone.Name = "btnDone";
            this.btnDone.Size = new System.Drawing.Size(87, 23);
            this.btnDone.TabIndex = 6;
            this.btnDone.Text = "Done";
            this.btnDone.UseVisualStyleBackColor = true;
            this.btnDone.Click += new System.EventHandler(this.btnDone_Click);
            // 
            // btnStartQueries
            // 
            this.btnStartQueries.Location = new System.Drawing.Point(352, 425);
            this.btnStartQueries.Name = "btnStartQueries";
            this.btnStartQueries.Size = new System.Drawing.Size(114, 23);
            this.btnStartQueries.TabIndex = 7;
            this.btnStartQueries.Text = "Start Queries Text Files";
            this.btnStartQueries.UseVisualStyleBackColor = true;
            this.btnStartQueries.Click += new System.EventHandler(this.btnStartQueries_Click);
            // 
            // btnStartFactual
            // 
            this.btnStartFactual.Location = new System.Drawing.Point(204, 425);
            this.btnStartFactual.Name = "btnStartFactual";
            this.btnStartFactual.Size = new System.Drawing.Size(114, 23);
            this.btnStartFactual.TabIndex = 8;
            this.btnStartFactual.Text = "Start Queries Factual";
            this.btnStartFactual.UseVisualStyleBackColor = true;
            this.btnStartFactual.Click += new System.EventHandler(this.btnStartFactual_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(603, 456);
            this.Controls.Add(this.btnStartFactual);
            this.Controls.Add(this.btnStartQueries);
            this.Controls.Add(this.btnDone);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtEditWindow);
            this.Controls.Add(this.txtJsonData);
            this.Controls.Add(this.txtQueryStatus);
            this.Name = "Form1";
            this.Text = "Async Factual Query";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtQueryStatus;
        private System.Windows.Forms.TextBox txtJsonData;
        private System.Windows.Forms.TextBox txtEditWindow;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnDone;
        private System.Windows.Forms.Button btnStartQueries;
        private System.Windows.Forms.Button btnStartFactual;
    }
}

