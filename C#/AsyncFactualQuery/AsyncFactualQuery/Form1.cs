﻿using FactualDriver;
using FactualDriver.Filters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AsyncFactualQuery
{
    public partial class Form1 : Form
    {
        string[] txtFiles = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.txt");
        const string MY_KEY = "2SHgpJM66rRpWiw4rY4DMIItsrCoICy6DPWdGnLk";
        const string MY_SECRET = "bUIF5BGmbnCsOXwo5f7hxhXcIbAuYYlS2dMa3Uw1";
        Factual factual;
            

        public class Rootobject
        {
            public int version { get; set; }
            public string status { get; set; }
            public Response response { get; set; }
        }

        public class Response
        {
            public Datum[] data { get; set; }
            public int included_rows { get; set; }
        }

        public class Datum
        {
            public string region { get; set; }
            public string tel { get; set; }
            public string[][] category_labels { get; set; }
            public string name { get; set; }
            public float longitude { get; set; }
            public string fax { get; set; }
            public string website { get; set; }
            public Hours hours { get; set; }
            public string[] neighborhood { get; set; }
            public string postcode { get; set; }
            public int[] category_ids { get; set; }
            public string country { get; set; }
            public string address { get; set; }
            public string locality { get; set; }
            public string hours_display { get; set; }
            public float latitude { get; set; }
            public string factual_id { get; set; }
            public float distance { get; set; }
            public string address_extended { get; set; }
            public string email { get; set; }
        }

        public class Hours
        {
            public string[][] monday { get; set; }
            public string[][] tuesday { get; set; }
            public string[][] wednesday { get; set; }
            public string[][] friday { get; set; }
            public string[][] thursday { get; set; }
            public string[][] saturday { get; set; }
            public string[][] sunday { get; set; }
        }

        public class GPSPoint
        {
            public string Longt { get; set; }
            public string Lat { get; set; }
            public string DateStamp { get; set; }
            public string Label { get; set; }


        }

        List<GPSPoint> gpsPoints;
        public Form1()
        {
            //intialize factual object 
            factual = new Factual(MY_KEY, MY_SECRET);
            factual.ConnectionTimeout = null;
            factual.ReadTimeout = null;
            InitializeComponent();
  

            //intialize a List of new GPSPoints 
            gpsPoints = new List<GPSPoint>
            {
                new GPSPoint
                {
                    Longt = "-81.224783",
                    Lat = "42.937437", 
                    Label = "FiveGuys",
                    DateStamp = DateTime.Now.ToShortDateString()
                },
                new GPSPoint{
                    Longt = "-81.22641",
                    Lat = "42.941681", 
                    Label = "Canada Computers",
                    DateStamp = DateTime.Now.ToShortDateString()
                },
                new GPSPoint{
                    Longt = "-81.253432",
                    Lat = "42.995198", 
                    Label = "Canada Computer Richmond",
                    DateStamp = DateTime.Now.ToShortDateString()
                },
                new GPSPoint{
                    Longt = "-81.25265769999999",
                    Lat = "42.99506359999999", 
                    Label = "P Za Pie",
                    DateStamp = DateTime.Now.ToShortDateString()
                },
                new GPSPoint{
                    Longt = "-81.25282980000003",
                    Lat = "42.9940214", 
                    Label = "Bic Souvlaki",
                    DateStamp = DateTime.Now.ToShortDateString()
                }

            };
        }

        private async void btnStartQueries_Click(object sender, EventArgs e)
        {
            int counter = 1;
            try
            {
                //looping through all files inside the folder 
                foreach (string file in txtFiles)
                {
                    txtQueryStatus.AppendText("Starting File Data Query" + counter + "\t" + DateTime.Now.TimeOfDay + "\n");
                    txtJsonData.Text = await JsonReturnFile(file);
                    counter++;
                }
               
                txtQueryStatus.AppendText("Done with File queries!!!\n");
            }catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        
        private async Task<string> JsonReturnFile(string file)
        {
            return await Task.Run(() =>
            {
                Thread.Sleep(4000);//testing purposes for waiting between files
                StreamReader readingFile = new StreamReader(File.Open(file, FileMode.Open));
               
                string readFile = readingFile.ReadToEnd();//read the file
               
                readingFile.Close();//close reader

                return readFile;//return file contents
                
            });
        }

        private async Task<string> JsonReturnFactual(GPSPoint gps)
        {
            
            return await Task.Run(() => 
            {
                 double lat = double.Parse(gps.Lat);
                 double lon = double.Parse(gps.Longt);

                //send query to data and save to a string, saving this is a json type string
                string factualJson = factual.Fetch("places", new Query().WithIn(new Circle(lat, lon, 2000)).Limit(10));
                string fileName = DateTime.Now.ToString("yy-dd-M--HH-mm-ss") + ".txt";
                //Write the string to a file
                StreamWriter file = new StreamWriter(fileName);

                file.WriteLine(factualJson);
                
                file.Close();
                return factualJson;//return the factual data

            });
        }

        
        private void btnDone_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private async void btnStartFactual_Click(object sender, EventArgs e)
        {
            int counter = 1;
            try
            {
                foreach (GPSPoint gps in gpsPoints)
                {
                    //loop throuhgh list of gps points and display the query starting in the status window
                    txtQueryStatus.AppendText("Starting Factual Data Query" + counter + "\t" + DateTime.Now.TimeOfDay + "\n");
                    //set the data returned from method and display it in the json data window
                    txtJsonData.Text = await JsonReturnFactual(gps);
                    counter++;
                }

                txtQueryStatus.AppendText("Done with Factual Queries queries!!!\n");
                //reintialize the files array for purposes after reading json saves to a file
                txtFiles = Directory.GetFiles(Directory.GetCurrentDirectory(), "*.txt");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        
    }
}
