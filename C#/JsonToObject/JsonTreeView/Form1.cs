﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace JsonTreeView
{
    public partial class frmConverter : Form
    {
        public frmConverter()
        {
            InitializeComponent();
        }

        private void frmConverter_Load(object sender, EventArgs e)
        {
            txtJSON.Text = @"{""airline"":""Oceanic"",""number"":815,""departure"":{""IATA"":""SYD"",""time"":""2004-09-22 14:55"",""city"":""Sydney""},""arrival"":{""IATA"":""LAX"",""time"":""2004-09-23 10:42"",""city"":""Los Angeles""}}";
        }

        private void btnConvert_Click(object sender, EventArgs e)
        {
            trvJson.Nodes.Clear();
            try
            {
                //dynamic object
                JObject json = JsonConvert.DeserializeObject<dynamic>(txtJSON.Text);
                //level counter
                int level = 0;
                //itterate through the json object
                foreach (var d in json)
                {
                    //if the count is not 0 will itterate throughthe key values
                    if (d.Value.Count() != 0)
                    {
                        //adds the key value
                        trvJson.Nodes.Add(d.Key.ToString().ToUpper());
                        //goes through values 
                        foreach (var k in d.Value.ToArray())
                        {
                            //take all quotes out
                            string t = k.ToString().Replace('"', '\n');
                            //add to the node level uner 
                            trvJson.Nodes[level].Nodes.Add(t);
                        }

                    }
                    else
                    {
                        //else just adds the key and value
                        trvJson.Nodes.Add(d.Key.ToString().ToUpper());
                        trvJson.Nodes[level].Nodes.Add(d.Value.ToString());
                    }
                    level++;

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void btnDone_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        
    }
}
