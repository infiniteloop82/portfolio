﻿using System;
using System.IO;            // File class
using System.Linq;

// Namespaces added manually
using System.Xml;           // XmlReader, XmlDocument and XmlReaderSetting classes
using System.Xml.Schema;    // XmlSchemaValidationFlags class

/*
Antonios Abraham
Feb 16 2016
    */

namespace XML_Console
{
    internal class Program
    {
        // Private member variables
        private static string xmlFile = "";

        private static bool valid_xml = true;

        private static void Main(string[] args)
        {
            Console.SetWindowSize(150, 60);
            try
            {
                // Display a title
                Console.WriteLine("WHMIS 2015 Label Information\n----------------------------\n");

                // Get the name of the XML file
                if (args.Count() > 0 && File.Exists(args[0]))
                {
                    // Getting XML file name from the command line
                    xmlFile = args[0];
                }
                else
                {
                    // Ask the user to input the file name
                    bool invalidFile = true;
                    do
                    {
                        Console.Write("Enter the path + name of your XML file: ");
                        xmlFile = Console.ReadLine();

                        if (!File.Exists(xmlFile))
                            Console.WriteLine("ERROR: The file '{0}' can't be found!", xmlFile);
                        else
                            invalidFile = false;
                    } while (invalidFile);

                    // Print a blank line
                    Console.WriteLine();
                }

                // Set the validation settings
                XmlReaderSettings settings = new XmlReaderSettings();
                settings.IgnoreComments = true; //added setting to ignor comments in the read out
                settings.ValidationType = ValidationType.Schema;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ReportValidationWarnings;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessInlineSchema;
                ValidationEventHandler handler
                        = new ValidationEventHandler(ValidationCallback);
                settings.ValidationEventHandler += handler;
                settings.ValidationFlags |= XmlSchemaValidationFlags.ProcessSchemaLocation;

                // Create the XmlReader object and read/validate the XML file
                XmlReader reader = XmlReader.Create(xmlFile, settings);

                // Load the xml into the DOM
                XmlDocument doc = new XmlDocument();
                doc.Load(reader);

                if (valid_xml)
                {
                    // ************************** ADD YOUR MAIN CODE HERE! **************************

                    // 1. Ask the user to specifiy which kind of label to display (supplier or workplace)

                    // 2. Display all fields in all labels of the type specified by the user

                    // (NOTE: You can do this in a loop and use the method Console.ReadKey() at the
                    // end of each iteration so that the user has to press a key to display the
                    // next label)
                    string option = "";
                    do
                    {
                        Console.Clear();
                        Console.WriteLine("Which Label would you like to display?\n(1) Supplier Label\n(2) Workplace Label");
                        option = Console.ReadLine();
                        switch (option)
                        {
                            case "1":
                                //codee for supplier labels
                                
                                GetLablesSupplier(doc);
                                break;

                            case "2":
                                //code for workplace labels
                                GetLableWorkplace(doc);
                                break;

                            default:
                                Console.WriteLine("\n{0} is not a valid command, please try again", option);
                                Console.ReadLine();
                                break;
                        }
                    } while (option != "1" && option != "2");
                }
            }
            catch (XmlException ex)
            {
                Console.WriteLine("ERROR: " + ex.Message);
            }

            // Hold the console window open until a key is pressed
            Console.WriteLine("\n\nPress any key to quit.");
            Console.ReadKey();
        } // end Main()

        // Callback method to display validation errors and warnings
        private static void ValidationCallback(object sender, ValidationEventArgs args)
        {
            if (args.Severity == XmlSeverityType.Warning)
                Console.WriteLine("WARNING: " + args.Message);
            else
            {
                Console.WriteLine("SCHEMA ERROR: " + args.Message);
                valid_xml = false;
            }
        } // end ValidationCall

        private static void GetLablesSupplier(XmlDocument doc)
        {
            XmlNodeList labelNodes = doc.GetElementsByTagName("label");
            
            foreach (XmlNode node in labelNodes)
            {
                XmlNodeList labelInfo = node.ChildNodes;
                //loop through each label and display the logic
                Console.WriteLine("\n------------------------------------------------------------------------------\n");
                //Loop through all attributes
                foreach (XmlNode nodes in labelInfo)
                {

                    // all fields not required by this type of label
                    if ((nodes.Name == "suppliment-label-info" && nodes.InnerText == "") || (nodes.Name == "msds-info")) { continue; }
                    if(nodes.Name == "pictogram")
                    {
                        Console.WriteLine("{0,-10}: \t{1,10}", nodes.Name.Replace('-', ' '), nodes.Attributes["pic"].Value);
                        continue;
                    }
                    Console.WriteLine("{0,-10}: \t{1,10}", nodes.Name.Replace('-', ' '), nodes.InnerText);
                }
                Console.ReadKey();
                //display table neatly
            }

            Console.WriteLine();
        }
        private static void GetLableWorkplace(XmlDocument doc)
        {
            XmlNodeList labelNodes = doc.GetElementsByTagName("label");
           
            foreach (XmlNode node in labelNodes)
            {
                XmlNodeList labelInfo = node.ChildNodes;
                //loop through each label and display the logic
                Console.WriteLine("\n------------------------------------------------------------------------------\n");
                //Loop through all attributes
                foreach (XmlNode nodes in labelInfo)
                {
                    // all fields not required by this type of label
                    if (nodes.Name == "suplimental-label-information" || nodes.Name == "initial-supplier-id"
                        || nodes.Name == "single-word" || nodes.Name == "hazard-statement"
                        || nodes.Name == "hazard-statement" || (nodes.Name == "msds-info" && nodes.InnerText == "" )) { continue; }
                    if (nodes.Name == "pictogram")
                    {
                        //get attribute to print out
                        Console.WriteLine("{0,-10}: \t{1,10}", nodes.Name.Replace('-', ' '), nodes.Attributes["pic"].Value);
                        continue;
                    }

                    Console.WriteLine("{0,-10}: \t{1,10}", nodes.Name.Replace('-', ' '), nodes.InnerText);
                }
                Console.ReadKey();
                //display table neatly
            }

            Console.WriteLine();
        }
    }
}