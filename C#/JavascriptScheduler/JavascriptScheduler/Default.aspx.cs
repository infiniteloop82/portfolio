﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

namespace JavascriptScheduler
{
    public partial class Default : System.Web.UI.Page
    {
        public static string dataPath = HttpContext.Current.Server.MapPath(".") + @"\data\";
        protected void Page_Load(object sender, EventArgs e)
        {

        }

          //returns gps location
        [WebMethod]
        public static string SaveAll(string memos, string tasks)
        {
            try
            {
                StreamWriter output = new StreamWriter(dataPath + "tasks.txt");

                output.WriteLine(tasks);

                output.Close();

                output = new StreamWriter(dataPath + "notes.txt");

                output.WriteLine(memos);

                output.Close();
            }
            catch (Exception ex)
            {

            }

            return "Saved";
        }
        


    }
}