﻿var days = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
var weeks = new Array(1, 2, 3, 4);
var tasks = [];
var memos = [];
var lastCell = null;


function makeTable() {
    $gel("scheduler").innerHTML = '<table>';
    for (var i = 0; i < days.length; i++) {

        $gel("scheduler").innerHTML += days[i];
    }

    $gel("scheduler").innerHTML += '</table>';
}

function checkOnline() {
    var isOnline = false;
    if( navigator.onLine){
        isOnline = true;
    }
    return isOnline;
}

function $gel(el) {
    return document.getElementById(el);
}

// create a 2D array which will store the grade values
// for each student within each course
for (var i = 0; i < weeks.length; i++) {
    tasks[i] = [];
    memos[i] = [];
    for (var j = 0; j < days.length; j++) {
        tasks[i][j] = "";
        memos[i][j] = "";
    }
    
}

// ************************************************
// event handler function is called whenever a user
// selects any cell in the table which has this
// handler attached
function selectCell(ref) {
    // extract the row and column of the cell
    // into rcArray
    var rcArray = ref.id.split("_");

    // if lastCell is not equal null, it means the user
    // has selected a cell previously and so we need to return the
    // background colour to white
    if (lastCell != null)
        lastCell.style.backgroundColor = "#E9F7FF";

    // set the currently selected cell (ref) to blue colour background
    ref.style.backgroundColor = "#FFF";

    // save the current reference to the global var lastCell
    lastCell = ref;

    document.getElementById("txtTask").value = ref.innerHTML;

    // set the value of the text box to whatever is in the selected cell
   
    document.getElementById("txtNote").value = memos[rcArray[0] - 1][rcArray[1] - 1];

    // set focus on the text box and select all the characters
    document.getElementById("txtTask").focus();
    document.getElementById("txtTask").select();
}

// ****************************************************
// event handler function initializes all grades to "0"
function clearAll() {
    for (var i = 0; i < weeks.length; i++) {
        for (var j = 0; j < days.length; j++)
            memos[i][j] = "";
    }

    refreshTable();
    $gel("txtNote").value = "";
    $gel("txtTask").value = "";
    // if the user has selected a cell, it will have the 
    // background colour reset to white
    if (lastCell != null)
        lastCell.style.backgroundColor = "#FFF";
}

// ********************************************************************
// onload event handler, automatically builds the dynamic table and all
// supporting data structures by calling the buildTable function
// we then call the refreshTable function to map data values in the 2D
// JS array to the HTML table
function initTable() {
    document.getElementById("container").innerHTML = buildTable(weeks.length, days.length);
    refreshTable();
}

// *********************************************************************
// helper function retrieves values from each element in the 2D array
// and assigns it (using innerHTML property) to a cell in the HTML table
function refreshTable() {
    for (var r = 0; r < memos.length; r++) {    
        var cid = "";
        var id = "";
        for (var c = 0; c < memos[r].length - 1; c++) {
            // construct the id value row and column with the "_" delimiter
            // Example: r = 0 and c = 0 --> "1_1"
            id = (r + 1) + "_" + (c + 1);
            document.getElementById(id).innerHTML = memos[r][c];           
        }
    }
    save();
    
}


// *******************************************************
// event handler function attached to the update button
// retrieves the value typed into the text box and assigns
// it to the relevant element in the 2D table
function updateArray() {
    // we can't do this if the user didn't select a cell
    // in the table
    if (lastCell === null)
        return;

    // retieve a reference to the cell the user selected
    // and then invoke the split method on the id property
    // for that reference cell to get the row and column
    // rcArray[0] contains the row
    // rcArray[1] contains the column
    var rcArray = lastCell.id.split("_");

    // if this is true the user selected a cell from the first column
    // which means a name cell
    // just assign this table cell directly with whatever the user
    // types into the text box
    if (rcArray[1] === "0") {
        lastCell.innerHTML = document.getElementById("txtTask").value;
       
    }
    else {
       
        // we need to subtract one from each of the row and column values to
        // map to the element in the grades array 
        var r = parseInt(rcArray[0]) - 1;
        var c = parseInt(rcArray[1]) - 1;

        // copy the value the user typed into the JS 2D array
        // notice that we must convert this string value (text boxes always contain strings)
        // to a numeric value
        tasks[r][c] = document.getElementById("txtTask").value;
        memos[r][c] = document.getElementById("txtNote").value;
        // invoke the refreshTable function to update the HTML table
        refreshTable();
    }
    
}

// ********************************************************************************
// helper function builds the string (divHTML) which contains all the dynamic
// HTML we want to inject into the page
// the finished string is then returned to the caller at the bottom of the function
function buildTable(rows, columns) {
    // create the initial table opening tag declaration
    var divHTML = '<table border="1" class="infoTable">';
    // open a new row with the <tr> tag for the headers
    divHTML += '<tr>'
    // define the 0,0 cell as a header because it's essentially empty
    // have a look at the table when the app is running and you can see
    // this cell in the upper left hand corner
    divHTML += '<th></th>';

    // now construct the headers using the courseNames array
    // notice that each entry has an opening <th> tag and a closing </th> tag around it
    for (var i = 0; i < days.length; i++)
        divHTML += "<th>" + days[i] + "</th>";

    // now we do a row of grades for each student
    for (var r = 0; r < rows; r++) {
        // opeing row tag...
        divHTML += "<tr>";
        // assemble a temporary string to ensure that the HTML you are building is syntactically correct
        // this will be the HTML which represents a cell in the table
        // the first one is used for the student names
        var temp = '<td id="' + (r + 1) + '_0" onclick="selectCell(this);" class="nameStyle">' + weeks[r] + "</td>";

        // add the temp variable containing the name cell HTML to the main string variable
        divHTML += temp;

        // now do all the grades for this student
        for (var c = 0; c < columns; c++) {
            // we're doing this directly without using a temporary variable to build the HTML for the cell
            // note that since our for loop counters start at 0, we need to add one to both the r and c values
            // in order to assemble a correct id for the cell
            // notice also that we can include references to JS event handlers and CSS styles so long as they
            // exist in our JS and CSS code respectively
            divHTML += '<td id="' + (r + 1) + '_' + (c + 1) + '" onclick="selectCell(this);" class="cellStyle"></td>';
        }
        // finally add the row closing tag
        divHTML += "</tr>";
    }

    // the last step is to add the table ending tag 
    divHTML += "</table>";

    // return the fully assembled string, ready to be inserted into
    // the div innerHTML property
    //alert(divHTML);
    return divHTML;
}


//functio to load data into the spreadsheet which call the ajax function to get the data stored in file
function load() {
    if (localStorage.memo != "" && localStorage.memo != localStorage.task) {
        var memo = JSON.parse(localStorage.memo);
        var task = JSON.parse(localStorage.task);
       
        for (var i in memo) {
            for (var j in memo[i]) {
                memos[i][j] = memo[i][j];
                tasks[i][j] = task[i][j];
            }
        }
        
    }
    refreshTable();
}

function save() {
    var jMemo = JSON.stringify(memos);
    var jTask = JSON.stringify(tasks);
    localStorage.setItem("memo", jMemo);
    localStorage.setItem("task", jTask);

    if (checkOnline()) {
        callAJAX("SaveAll", jMemo, jTask);
    }

    $gel("txtTask").value = "";
    $gel("txtNote").value = "";
}

var hostPage = "default.aspx/";
// Main AJAX entry point
function callAJAX(requestMethod, memos, tasks) {
    var pageMethod = hostPage + requestMethod;
    var jobj = { "memos": memos, "tasks": tasks };
    $.ajax({
        url: pageMethod,   // Current Page, Method - default.aspx/
        data: JSON.stringify(jobj), // parameter map as JSON 
        type: "POST", // data has to be POSTed  
        contentType: "application/json", // posting JSON content      
        dataType: "JSON",  // type of data is JSON (must be upper case!)  
        timeout: 600000,    // AJAX timeout  
        success: function (result) {
            ajaxCallback(result.d);
        },
        error: function (xhr, status) {
            alert(status + " - " + xhr.responseText);
        }
    });
}

function ajaxCallback(response) {
    $gel("stats").innerHTML = response;

}