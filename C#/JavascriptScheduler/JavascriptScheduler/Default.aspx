﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="JavascriptScheduler.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="scripts/jquery-1.11.3.min.js"></script>
    <script src="scripts/EventUtil.js"></script>
    <script src="scripts/main.js"></script>
    <link href="styles/main.css" rel="stylesheet" />
    <title>Scheduler</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="The description of my page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body onload="initTable();">
    
    <form id="form1" runat="server">

    <div id="wrap">
        <h1>Scheduler</h1>
         <div id="container">
    </div>
        <div id="scheduler"></div>
        <div id="notes"></div>
         <input type="text" id="txtTask" />
            <br />
        <textarea rows="5" cols="50" id="txtNote"></textarea>
         <br />
        <br />

    <input id="btnUpdate" type="button" value="Save" onclick="updateArray();" />
    <input id="btnClear" type="button" value="Clear Grid" onclick="clearAll();" />
        
    <h3 id="stats"></h3>
    </div>
    
    </form>
</body>
</html>
