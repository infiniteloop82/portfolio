﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="JavascriptScheduler.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <script src="scripts/main.js"></script>
    <link href="styles/main.css" rel="stylesheet" />
    <title></title>
</head>
<body onload="makeTable();">
    <form id="form1" runat="server">
    <div id="wrap">
        <div id="scheduler"></div>
        <div id="notes"></div>
    </div>
    </form>
</body>
</html>
