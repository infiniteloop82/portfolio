// JavaScript for MLBDemo

var baseballObject;
// ***********************************
// AJAX XMLHttpRequest to get the JSON
// from the site defined by url
function getJSON(url) {
    var resp;
    var xmlHttp;

    resp = '';
    xmlHttp = new XMLHttpRequest();
    try {
       

            xmlHttp.open("GET", url, false);
            xmlHttp.send(null);
            resp = xmlHttp.responseText;
       

        return resp;
    } catch (err) {
        alert('NO DATA FOUND\nYou mah have entered an incorrect date.\nPlease check and try again!');
        
    }
}

// ************************************
// onload event handler creates the URL
// for a given year month and day
function getBaseballData() {

    //gets value from json file from the path
    var baseballJson = getJSON("http://gd2.mlb.com/components/game/mlb/year_" + $gel("txtYear").value + "/month_" + $gel("txtMonth").value + "/day_" + $gel("txtDay").value + "/master_scoreboard.json");
    //var baseballJson = getJSON("http://gd2.mlb.com/components/game/mlb/year_2010/month_06/day_10/master_scoreboard.json");

    //parses the json file for javascript object
    baseballObject = JSON.parse(baseballJson);
    formatData(baseballObject);
}

//formats object to deploy data into table from json string
function formatData(jObj) {

    var games = jObj.data.games.game;
    data = "<tr><th>Game #</th><th>Home Team</th><th>Away Team</th></tr>";

    //Loop through to find if toronro exists in this day of games
    for (var i = 0; i < games.length; i++) {
        //if found in away or home
        if (games[i].home_team_city == "Toronto" || games[i].away_team_city == "Toronto") {
            //sets game to a var
            var toronto = games[i];
            //removes it from array
            games.splice(i, 1);
            //the adds it to the front of the array
            games.unshift(toronto);
            //alert(games[i].home_team_name);
        }
    }
    document.getElementsByTagName("section").innerHTML = "<p>Gammes for " + games.date
    //loops through array and adds rows of games to a variable
    for (var i = 0; i < games.length; i++) {
        data += "<tr>";
        data += "<td>" + (i + 1) + "</td>";
        data += "<td>" + games[i].home_team_city + " " + games[i].home_team_name.replace(games[i].home_team_city, '') + "</td>";
        data += "<td>" + games[i].away_team_city + " " + games[i].away_team_name.replace(games[i].away_team_city, ''); + "</td>";

        data += "</tr>";
    }
    //sets table with the data variable
    $gel("tblResults").innerHTML = data;

}

//called on page start up
function init() {
    var year = "2015";
    var month = "03";
    var day = "06";
    //sets defaukt values to text boxes
    $gel("txtMonth").defaultValue = month;
    $gel("txtDay").defaultValue = day;
    $gel("txtYear").defaultValue = year;

}


function $gel(el){
    return document.getElementById(el);
}