<?php

/* constants */

define("host", "localhost");
define("user", "tester");
define("password", "tester");
define("db", "lamp2proj1");

//creating connection object
$conn = new mysqli(host, user, password, db);
//print_r($conn);
//check connecion
if ($conn->connect_errno != 0) {
    echo "Failed to connect!";
    die();
}

function insertTask($task) {
    //define as global to user vars outside a function
    global $conn;
    //create insert query 
    $query = "INSERT INTO task (description, priority) VALUES ('" . addslashes($task['description']) . "', " . $task['priority'] . ")";
    //execute query
    if ($conn->query($query) === TRUE) { //check insert
        return TRUE; //reutrn true if success
    } else {
        return FALSE;
    }
}

function updateTask($id) {
    global $conn;
    //create update query
    $dateNow = date("Y-m-d H:i:s");
    $query = "UPDATE task SET completed = 1, dateCompleted = '" . $dateNow . "'WHERE id = " . $id;
    if ($conn->query($query) > 0) {
        //return insert ID id successful
        return $conn->insert_id;
    } else {
        return FALSE;
    }
}

function deleteTask($id) {
    global $conn;
    //create delete query
    $query = "DELETE FROM task WHERE id = " . $id;
    if ($conn->query($query) === TRUE) {
        //return true
        return TRUE;
    } else {
        return FALSE;
    }
}

function getAllTasks($task) {
    global $conn;
    // get all recrods from database
    $dateCreated = "SELECT * FROM task ORDER BY dateCreated DESC";
    $priority = "SELECT * FROM task ORDER BY priority DESC";
    $incompletePriority = "SELECT * FROM task WHERE completed = 0 ORDER BY priority DESC";
    $incompleteDate = "SELECT * FROM task WHERE completed = 0 ORDER BY dateCreated DESC";
    $query = $dateCreated;
    switch ($task) {
        case 'priority':
            $query = $priority;
            break;
        case 'incompletePriority':
            $query = $incompletePriority;
            break;
        case 'incompleteDate':
            $query = $incompleteDate;
            break;
        case 'allDate':
            $query = $dateCreated;
            break;
        default:
            $query = $dateCreated;
            break;
    }
    //create the return json array
    $json = array();
    //execute query
    $rs = $conn->query($query);
    if ($rs) {
        //return record set
        while ($row = $rs->fetch_assoc()) {
            $json[] = $row;
        }
        //encode to json to send back
        $json = json_encode($json);
        return $json;
    } else {
        return FALSE;
    }
    //check for record id and rows affected 
}
