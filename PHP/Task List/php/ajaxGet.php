<?php
header("Content-Type: application/json");
session_start();
require_once('dbConn.php');

$action = $_REQUEST['action'];
$task = $_REQUEST['task'];

//list of tasks
//$tasks = array();

if ($_SERVER['REQUEST_METHOD'] === 'POST' || $_SERVER['REQUEST_METHOD'] === 'GET') {
    if (!empty($action) && isset($action)) {
        switch ($action) {
            case "list":
                //get all records
                $records = getAllTasks($task);
                echo $records;
                break;
            case "new":
                //insert record
                $result = insertTask($task);
                if ($result) {
                    echo json_encode("{'Success' : 'true'}");
                } else {
                    echo json_encode("{'Success' : 'false'}");
                }
                break;
            case "delete":
                //deletrecord
                $list = json_decode($task);
                for ($i = 0; $i < count($list); $i++) {
                    //call function to delete in dbConnect.php
                    $del[] = deleteTask($list[$i]);
                }
                if (count($del) > 0) {
                    echo json_encode("{'Success' : 'true'}");
                } else {
                    echo json_encode("{'Success' : 'false'}");
                }
                break;
            case "complete":
                //update
                $list = json_decode($task);
                for ($i = 0; $i < count($list); $i++) {
                    $del[] = updateTask($list[$i]);
                }
                if (count($del) > 0) {
                    echo json_encode("{'Success' : 'true'}");
                } else {
                    echo json_encode("{'Success' : 'false'}");
                }
                break;
                
        }
    }
}