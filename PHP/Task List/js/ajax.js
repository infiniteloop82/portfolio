//task object
var task = {
    "description": "",
    "priority": 0
};
//file for php functions
var ajaxFile = "php/ajaxGet.php";

//table heading
var tableHead = ['Select', 'Description', 'Priority', 'Completed', 'Created'];
//priority display
var priority = ['', 'Very Low', 'Low', 'Normal', 'High', 'Very High'];

//sorting variable
var sqlTask = "";

/*
 * Finish aall sorting preocesses 
 */

$gel("radAll").checked = "checked";
$gel("radDate").checked = "checked";

var rsTable = [];

//onload page function
function init(order) {
    //get task list from database
    //load task list on startup
    if (!order) {
        order = sqlTask;
    }
    console.log(order);
    $.get(ajaxFile, {'action': "list", 'task': order}, function (data) {
        //var jdec = JSON.parse(data);

        rsTable = data;
        console.log(data);
        createTable(data);
        status(data);
    });
    console.log(rsTable);
    //clear all field funtion
    clearFeilds();

}

function taskDelete() {
    //delete task send command through ajax

    var ids = getAllCheckedBoxes();
    var jsonId = JSON.stringify(ids);
    console.log(jsonId);
    $.post(ajaxFile, {'action': "delete", 'task': jsonId}, function (data) {
        console.log(data);
        status(data);
        init();
    });
}


function submitTask() {
    //send json string task info through ajax

    //checks to makke sure something is in the text box
    if ($gel("txtTask").value !== "") {
        //set json object
        task.description = $gel("txtTask").value;
        var prio = $gel("cmbPriority");
        task.priority = prio.options[prio.selectedIndex].value;
        //sending a post ajax request with task object
        $.post(ajaxFile, {'action': "new", 'task': task}, function (data) {
            console.log(data);
            status(data);
            init();
        });
    } else {
        alert('Your task is empty, try entering a task first.');
        clearFields();
    }
}

function taskComplete() {
    //cross out completed task css

    //update db set completed=1 | true;
    //delete task send command through ajax
    //gets all the check boxes
    var ids = getAllCheckedBoxes();
    //creates a string to send to the ajax call
    var jsonId = JSON.stringify(ids);
    console.log(jsonId);
    $.post(ajaxFile, {'action': "complete", 'task': jsonId}, function (data) {
        console.log(data);
        status(data);
        init();

    });
}


//function just to get all checked boxes 
function getAllCheckedBoxes() {
    var checkBoxes = document.getElementsByName('id');
    var checkedBoxes = [];
    for (var i = 0; i < checkBoxes.length; i++) {
        if (checkBoxes[i].checked) {
            checkedBoxes.push(parseInt(checkBoxes[i].value));
        }
    }
    console.log(checkedBoxes);
    return checkedBoxes;
}


function createTable(json) {
    var tasks;// = array();
    //get all the records
    tasks = json;

    //Create a HTML Table element.
    var table = document.createElement("table");
    //table.border = "1";

    //Get the count of columns.
    var columnCount = tableHead.length;
    //console.log(tasks[0]);

//Add the header row.
    var row = table.insertRow(-1);
    for (var i = 0; i < columnCount; i++) {

        var headerCell = document.createElement("TH");
        headerCell.innerHTML = tableHead[i];
        row.appendChild(headerCell);

    }
    //Add the data rows.
    for (var i = 0; i < tasks.length; i++) {
        row = table.insertRow(-1);
        //sets class if row is completed
        if (tasks[i].completed === '1') {
            row.className = "complete";

        }
        //sets all cells
        var id = row.insertCell(-1);
        var description = row.insertCell(-1);
        var priority = row.insertCell(-1);
        var completed = row.insertCell(-1);
        var created = row.insertCell(-1);

        //sets the cell with all the data
        id.innerHTML = "<input type='checkbox' name='id' value='" + tasks[i].id + "' />";
        description.innerHTML = tasks[i].description;
        priority.innerHTML = priorityToText(tasks[i].priority);
        completed.innerHTML = tasks[i].dateCompleted;
        created.innerHTML = tasks[i].dateCreated;


    }
    console.log(table);
    var dtable = document.getElementById("description");
    dtable.innerHTML = "";
    dtable.appendChild(table);
    console.log(dtable);
}


function clearFeilds() {
    $gel("txtTask").value = "";
    $gel("cmbPriority").value = 3;
    $gel("txtTask").focus();
}

function taskToDisplay(display) {
    //sets task to execute query on php side
    if ((display === 'all'  && $gel("radPriority").checked) || (display === 'priority' && $gel("radAll").checked )) {
        sqlTask = 'priority';
        init('priority');
    } else if ((display === 'incomplete' && $gel("radPriority").checked) || (display === 'priority' && $gel("radIncomplete").checked) ) {
        sqlTask = 'incompletePriority';
        init('incompletePriority');
    } else if ((display === 'date' && $gel("radIncomplete").checked) || ((display === 'incomplete' && $gel("radDate").checked))) {
        sqlTask = 'incompleteDate';
        init('incompleteDate');
    }else if ((display === 'date'  && $gel("radAll").checked) || (display === 'all' && $gel("radDate").checked )){
        sqlTask = 'allDate';
        init('allDate');
    }
    
}


function status(stat) {
    var status = $gel('status');
    status.className = "success";
    status.innerHTML = "Success";
    if (stat.success === "false") {
        status.className = "fail";
        status.innerHTML = "Fail!!!";
    }

}


//add a class of complete to functions alread complete
//NOT USED 
function addCompleteClass(data) {
    return "<span class='complete'>" + data + "</span>";
}

function priorityToText(index) {
    return priority[index];
}




//helper methods
function $gel(el) {
    return document.getElementById(el);
}